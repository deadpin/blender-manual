.. index:: Geometry Nodes; Curve Tip

*********
Curve Tip
*********

Reads information about each curve's tip point.

.. peertube:: pcyUMH2SZ2DtwuDVF1bQio


Inputs
======

This node has no inputs.


Properties
==========

This node has no properties.


Outputs
=======

Tip Selection
   Boolean selection of curve tip points.

Tip Position
   Position of the tip point of a Curve.

Tip Direction
   Direction of the tip segment of a Curve.

Tip Index
   Index of the tip point of a Curve.
